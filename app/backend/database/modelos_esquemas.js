const Esquema=require("mongoose").Schema;
/* Existe el esquema de pacientes y el esquema paciente el cual es hijo de pacientes;sin embargo, ambos pertenecen a la colección pacientes */

const   Paciente_Esquema=new Esquema({
    dni:{type:String,unique:true,required:true},
    nombre:{type:String,required:true},
    a_paterno:{type:String,required:true},
    a_materno:{type:String,required:true},
    especialidad:{type:String,required:true},
    estado:{type:String,enum:["Pendiente","Resuelto","Derivado"],required:true},
    imagen:{type:Buffer}
},{collection:"pacientes"})

module.exports={
    paciente:require("mongoose").model("Paciente",Paciente_Esquema)
}