const rutas=require("express").Router();
const controlador_bbdd=require("../controllers/control_bbdd");

rutas.get("/",(req,res)=>{
    res.render("index");
})

rutas.get("/inicio",(req,res)=>{
    pacientes=controlador_bbdd.lista_pacientes();
    res.render("./templates/usuario",{pacientes:pacientes})
})


module.exports=rutas;
